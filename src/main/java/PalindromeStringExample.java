package com.devops.palindrome;

class PalindromeStringExample{  
	public static void main(String args[]){  

		String input=args[0];
		System.out.println("Input Value is: "+input);

		StringBuilder output = new StringBuilder();
		output.append(input);
		output.reverse();
		System.out.println("Output(reverse) value is: "+output);

		if(input.equals(output.toString())){
			System.out.println("This is palindrome String: TRUE");
		}else{
			System.out.println("This is not palindrome String: FALSE");
		}
	}  
}

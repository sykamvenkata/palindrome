FROM openjdk:8-jdk-alpine

COPY target/*.jar /palindrome.jar
ENTRYPOINT ["java","-jar","/palindrome.jar"]
CMD ["AHA"]

